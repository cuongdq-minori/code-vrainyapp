import { Unknown } from "./Unknown";

export interface City extends Unknown {
  name: string;
  id: string;
  rainyAround?: number;
  status: string;
  provinces?: Provinces[];
}
export interface Provinces extends Unknown {
  name: string;
  id: string;
  villageName: string;
  rainyAround?: number;
  status: string;
  area?: string;
}
