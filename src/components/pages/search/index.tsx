import React, { useLayoutEffect, useState } from "react";
import { FlatList, RefreshControl, SafeAreaView as View } from "react-native";
import { Chip, Divider, Searchbar, Text } from "react-native-paper";
import { City } from "../../../interfaces/interface";
import { SearchProps } from "../../../navigator";
import { DataSource } from "../home/data";
import { styles } from "./style";

const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};

export default function Search(props: SearchProps) {
  const [name, setName] = useState("");
  const [dataSource, setDataSource] = useState<City[]>([]);
  const [refreshing, setRefreshing] = React.useState(false);
  const [selected, setSelected] = React.useState({
    heavy: false,
    little: false,
    no: false,
  });

  useLayoutEffect(() => {
    props.navigation.setOptions({
      headerTitle: "Tìm kiếm",
    });
  }, []);

  const onHandleSearch = (text: string) => {
    setName(text);
    setDataSource(DataSource);
  };

  const onClickToDetail = (detail: any) => {
    props.navigation.navigate("DistrictList", {
      item: detail.provinces,
      headerName: detail.name,
    });
  };

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(1000)
      .then(() => setRefreshing(false))
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const onClearSearch = React.useCallback(() => {
    setDataSource([]);
    setName("");
  }, []);
  return (
    <View style={styles.search}>
      <Searchbar
        value={name}
        style={styles.searchBar}
        onChangeText={(text) => onHandleSearch(text)}
      />
      <View style={styles.searchStatus}>
        <Chip
          selected={selected.heavy}
          selectedColor="green"
          style={styles.marginTag}
          onPress={() => setSelected({ ...selected, heavy: !selected.heavy })}
        >
          Mưa nhiều
        </Chip>
        <Chip
          selected={selected.little}
          selectedColor="green"
          style={styles.marginTag}
          onPress={() => setSelected({ ...selected, little: !selected.little })}
        >
          Không mưa
        </Chip>
        <Chip
          selected={selected.no}
          selectedColor="green"
          style={styles.marginTag}
          onPress={() => setSelected({ ...selected, no: !selected.no })}
        >
          ít mưa
        </Chip>
      </View>
      {dataSource.length > 0 ? (
        <View style={styles.marginTag}>
          <View style={styles.searchContent}>
            <Text style={styles.textHeaderLeft}>Nội dung tìm kiếm</Text>
            <Text style={styles.textHeaderRight} onPress={onClearSearch}>
              Xóa
            </Text>
          </View>
          <Divider style={styles.divider} />
          <FlatList
            refreshing={true}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
            data={dataSource}
            keyExtractor={(item) => item.id}
            renderItem={({ item, index }) => (
              <View key={index}>
                <Text onPress={() => onClickToDetail(item)} style={styles.item}>
                  {item.name}
                </Text>
                <Divider style={styles.divider} />
              </View>
            )}
          />
        </View>
      ) : (
        <View style={styles.marginTag}>
          <View style={styles.searchContent}>
            <Text style={styles.textHeaderLeft}>Nội dung mới tìm kiếm</Text>
            <Text style={styles.textHeaderRight}>Xóa</Text>
          </View>
          <Divider style={styles.divider} />
          <FlatList
            refreshing={true}
            data={[
              { key: "Hà nội" },
              { key: "Hà tĩnh" },
              { key: "Đà nẵng" },
              { key: "Jackson" },
            ]}
            renderItem={({ item, index }) => (
              <View key={index}>
                <Text style={styles.item}>{item.key} </Text>
                <Divider style={styles.divider} />
              </View>
            )}
          />
        </View>
      )}
    </View>
  );
}
