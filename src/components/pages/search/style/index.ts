import { StyleSheet } from "react-native";
export const styles = StyleSheet.create({
  search: {
    flex: 1,
    backgroundColor: "#424c61",
  },
  searchBar: { margin: 5, marginTop: 10 },
  searchStatus: {
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: 10,
  },
  marginTag: { margin: 5 },
  searchContent: {
    flexDirection: "row",
    justifyContent: "space-between",
    margin: 10,
  },
  textHeaderLeft: { color: "white", fontWeight: "bold", fontSize: 16 },
  textHeaderRight: { color: "white", fontWeight: "bold", fontSize: 14 },
  divider: { backgroundColor: "#897272" },
  item: {
    padding: 10,
    fontSize: 14,
    color: "white",
  },
});
