import { StyleSheet } from "react-native";
import { colors } from "../../../constants/color";

export const styles = StyleSheet.create({
  back: {
    width: "100%",
    marginTop: 12,
  },
  button: {
    marginTop: 12,
  },
  label: {
    color: colors.secondary,
    width: "100%",
  },
  textHeader: {
    fontSize: 26,
    color: colors.primary,
    fontWeight: "bold",
    paddingVertical: 14,
  },
});
