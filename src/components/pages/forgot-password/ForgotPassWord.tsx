import React from "react";
import { SafeAreaView, ScrollView } from "react-native";
import { BackgroundBase } from "../../../components/atoms/background/index";
import { TextHeader } from "../../../components/atoms/text/index";
import { Logo } from "../../../components/molecules/logo/index";
import { ForgotPassProps } from "../../../navigator";
import { FormForgotPassword } from "../../organisms/form-input/FormForgotPassword";
import { styles } from "./style/index";

export const ForgotPassword = (props: ForgotPassProps): JSX.Element => {
  return (
    <SafeAreaView>
      <ScrollView>
        <BackgroundBase
          resizeMode="repeat"
          source={require("../../../assets/background_dot.png")}
        >
          <Logo />
          <TextHeader StyleSheet={styles.textHeader}>Quên mật khẩu</TextHeader>
          <FormForgotPassword {...props} />
        </BackgroundBase>
      </ScrollView>
    </SafeAreaView>
  );
};
