import React, { useLayoutEffect } from "react";
import { RefreshControl, SafeAreaView, ScrollView, Text } from "react-native";
import { DetailProps } from "../../../navigator";
import { ButtonAction, IconButton } from "../../atoms/button";
import { CardDetail } from "../../molecules/card/index";
import { Map } from "../../molecules/map/index";
import styles from "./style/detail";
const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};

export const Detail = (props: DetailProps): JSX.Element => {
  const { headerName, detail } = props.route.params;
  const [refreshing, setRefreshing] = React.useState(false);

  useLayoutEffect(() => {
    props.navigation.setOptions({
      headerTitle: headerName,
      headerRight: () => (
        <IconButton
          icon="home"
          mode="text"
          children={false}
          color="white"
          labelStyle={{ fontSize: 26 }}
          onPress={() => props.navigation.navigate("Home")}
          style={{ marginRight: 10 }}
        />
      ),
      headerRightContainerStyle: {},
    });
  }, []);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);

    wait(2000).then(() => setRefreshing(false));
  }, []);

  return (
    <SafeAreaView style={styles.detail}>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <Map style={styles.mapConainer} />
        <CardDetail style={styles.cardContainer} detail={detail} />
        <Text style={styles.executionText}>20/9/2019 - 20/11/2020</Text>
        <ButtonAction
          mode="contained"
          style={styles.btnResetData}
          onPress={onRefresh}
        >
          Cập nhật
        </ButtonAction>
      </ScrollView>
    </SafeAreaView>
  );
};
