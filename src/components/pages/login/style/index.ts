import { StyleSheet } from "react-native";
import { colors } from "../../../constants/color";

const styles = StyleSheet.create({
  textHeader: {
    fontSize: 26,
    color: colors.primary,
    fontWeight: "bold",
    paddingVertical: 14,
  },
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "stretch",
    padding: 5,
    backgroundColor: "white",
  },
});

export default styles;
