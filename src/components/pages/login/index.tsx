import React, { useLayoutEffect } from "react";
import { SafeAreaView, ScrollView } from "react-native";
import { BackgroundBase } from "../../../components/atoms/background/index";
import { TextHeader } from "../../../components/atoms/text/index";
import { Logo } from "../../../components/molecules/logo/index";
import { LoginProps } from "../../../navigator";
import { FormInputLogin } from "../../organisms/form-input/FormInputLogin";
import styles from "./style/index";

export const LoginAuthor = (props: LoginProps): JSX.Element => {
  useLayoutEffect(() => {
    props.navigation.setOptions({
      headerShown: false,
      headerStatusBarHeight: 1,
    });
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <BackgroundBase
          resizeMode="repeat"
          source={require("../../../assets/background_dot.png")}
        >
          <Logo />
          <TextHeader StyleSheet={styles.textHeader}>Welcome back.</TextHeader>
          <FormInputLogin {...props} />
        </BackgroundBase>
      </ScrollView>
    </SafeAreaView>
  );
};
