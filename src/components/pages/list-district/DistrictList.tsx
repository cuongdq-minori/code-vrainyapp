import React, { useLayoutEffect } from "react";
import { RefreshControl, SafeAreaView, ScrollView } from "react-native";
import { Provinces } from "../../../interfaces/interface";
import { DistrictListProps } from "../../../navigator";
import { IconButton } from "../../atoms/button";
import { ListItem } from "../../organisms/list/ListItem";
import styles from "./style";

const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};
export const DistrictList = (props: DistrictListProps): JSX.Element => {
  const { item: DetailCity, headerName } = props.route.params;

  const onPressToDetail = (district: Provinces) => {
    props.navigation.navigate("Detail", {
      headerName: district.name,
      detail: district,
    });
  };

  useLayoutEffect(() => {
    props.navigation.setOptions({
      headerTitle: headerName,
      headerRight: () => (
        <IconButton
          icon="magnify"
          mode="text"
          children={false}
          color="white"
          labelStyle={{ fontSize: 26 }}
          onPress={() => props.navigation.navigate("Search")}
        />
      ),
    });
  }, []);

  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);

    wait(2000).then(() => setRefreshing(false));
  }, []);

  return (
    <SafeAreaView style={styles.listDistrict}>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <ListItem DataSource={DetailCity} onPress={onPressToDetail}></ListItem>
      </ScrollView>
    </SafeAreaView>
  );
};
