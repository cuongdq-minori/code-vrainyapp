import React, { memo } from "react";
import { SafeAreaView, ScrollView } from "react-native";
import { BackgroundBase } from "../../../components/atoms/background/index";
import { TextHeader } from "../../../components/atoms/text/index";
import { RegisterProps } from "../../../navigator";
import { FormInputRegister } from "../../organisms/form-input/FormInputRegister";

const RegisterScreen = (props: RegisterProps) => {
  return (
    <SafeAreaView>
      <ScrollView>
        <BackgroundBase
          resizeMode="repeat"
          source={require("../../../assets/background_dot.png")}
        >
          <TextHeader>Tạo tài khoản</TextHeader>
          <FormInputRegister {...props} />
        </BackgroundBase>
      </ScrollView>
    </SafeAreaView>
  );
};

export default memo(RegisterScreen);
