import auth, { FirebaseAuthTypes } from "@react-native-firebase/auth";
import messaging from "@react-native-firebase/messaging";
import * as React from "react";
import { useEffect, useLayoutEffect, useState } from "react";
import {
  Alert,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  View,
} from "react-native";
import { City } from "../../../interfaces/interface";
import { HomeProps } from "../../../navigator";
import { IconButton } from "../../atoms/button";
import { ListItem } from "../../organisms/list/ListItem";
import { DataSource } from "./data";
import styles from "./style/styles";

const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};

export const Home = (props: HomeProps): JSX.Element => {
  const [user, setUser] = useState<FirebaseAuthTypes.User | null>(null);
  const [refreshing, setRefreshing] = React.useState(false);

  const onPressToListTown = (city: City) => {
    props.navigation.navigate("DistrictList", {
      item: city.provinces,
      headerName: city.name,
    });
  };

  const onHandlerConfirm = () => {
    Alert.alert(
      "Confirm Logout",
      "Bạn muốn đăng xuất khỏi thiết bị này?",
      [
        {
          text: "Đăng xuất",
          onPress: () => {
            auth()
              .signOut()
              .then(() => console.log("User signed out!"));
          },
        },
        {
          text: "Hủy",
          style: "cancel",
        },
      ],
      { cancelable: false }
    );
  };

  useLayoutEffect(() => {
    props.navigation.setOptions({
      gestureEnabled: false,
      headerRight: () => (
        <View style={{ flexDirection: "row" }}>
          <IconButton
            icon="magnify"
            mode="text"
            children={false}
            color="white"
            labelStyle={{ fontSize: 26 }}
            onPress={() => props.navigation.navigate("Search")}
          />
          <IconButton
            icon="logout"
            children={false}
            mode="text"
            color="white"
            labelStyle={{ fontSize: 26 }}
            onPress={onHandlerConfirm}
          />
        </View>
      ),
    });
  }, []);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(1000)
      .then(() => setRefreshing(false))
      .catch((error) => {
        console.log(error);
      });
  }, []);

  useEffect(() => {
    // auth().onAuthStateChanged((userState) => {
    //   setUser(userState);
    //   if (!user) props.navigation.navigate("Login");
    // });
    const unsubscribe = messaging().onMessage(async (remoteMessage) => {
      Alert.alert("A new FCM message arrived!", JSON.stringify(remoteMessage));
    });
    return unsubscribe;
  }, []);

  return (
    <SafeAreaView style={styles.home}>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <ListItem
          DataSource={DataSource}
          onPress={onPressToListTown}
        ></ListItem>
      </ScrollView>
    </SafeAreaView>
  );
};
