import React from "react";
import { View } from "react-native";
import { Any } from "../../../interfaces/Unknown";
import { Item } from "../../molecules/item/index";
import { styles } from "./style/list-item";
interface ListItemProps {
  DataSource?: Any[];
  onPress: (value: Any) => void;
}
export const ListItem = ({
  DataSource,
  onPress,
}: ListItemProps): JSX.Element => {
  return (
    <View style={styles.listItem}>
      {DataSource?.map((city, i) => (
        <Item key={i} data={city} onPress={onPress}></Item>
      ))}
    </View>
  );
};
