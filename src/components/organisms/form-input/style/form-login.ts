import { StyleSheet } from "react-native";
import { colors } from "../../../constants/color";
export const styles = StyleSheet.create({
  forgotPassword: {
    width: "100%",
    alignItems: "flex-end",
    marginBottom: 24,
  },
  row: {
    flexDirection: "row",
    marginTop: 4,
  },
  label: {
    color: colors.secondary,
  },
  link: {
    fontWeight: "bold",
    color: colors.primary,
  },
});
