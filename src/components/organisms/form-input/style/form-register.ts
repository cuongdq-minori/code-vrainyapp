import { StyleSheet } from "react-native";
import { colors } from "../../../constants/color";
export const styles = StyleSheet.create({
  label: {
    color: colors.secondary,
  },
  button: {
    marginTop: 24,
  },
  row: {
    flexDirection: "row",
    marginTop: 4,
  },
  link: {
    fontWeight: "bold",
    color: colors.primary,
  },
});
