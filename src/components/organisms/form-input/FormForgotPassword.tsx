import React, { Fragment, useState } from "react";
import { Text, TouchableOpacity } from "react-native";
import { ButtonAction } from "../../../components/atoms/button/index";
import { TextInput } from "../../../components/molecules/text-input/index";
import { ForgotPassProps } from "../../../navigator";
import { styles } from "./style/form-forgot-password";

export const FormForgotPassword = (props: ForgotPassProps): JSX.Element => {
  const [email, setEmail] = useState({ value: "", error: "" });

  const _onSendPressed = () => {
    props.navigation.navigate("Login");
  };

  return (
    <Fragment>
      <TextInput
        label="E-mail address"
        returnKeyType="done"
        value={email.value}
        onChangeText={(text) => setEmail({ value: text, error: "" })}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      />
      <ButtonAction
        mode="contained"
        onPress={_onSendPressed}
        style={styles.button}
      >
        Xác nhận mật khẩu
      </ButtonAction>

      <TouchableOpacity
        style={styles.back}
        onPress={() => props.navigation.navigate("Login")}
      >
        <Text style={styles.label}>← Trở về</Text>
      </TouchableOpacity>
    </Fragment>
  );
};
