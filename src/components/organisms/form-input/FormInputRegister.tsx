import React, { Fragment, useState } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import { RegisterProps } from "../../../navigator";
import { ButtonAction } from "../../atoms/button/index";
import { TextInput } from "../../molecules/text-input/index";
import { styles } from "./style/form-register";

export const FormInputRegister = (props: RegisterProps) => {
  const [name, setName] = useState({ value: "", error: "" });
  const [email, setEmail] = useState({ value: "", error: "" });
  const [password, setPassword] = useState({ value: "", error: "" });

  const _onSignUpPressed = () => {
    props.navigation.navigate("Login");
  };

  return (
    <Fragment>
      <TextInput
        label="Họ & Tên"
        returnKeyType="next"
        value={name.value}
        onChangeText={(text) => setName({ value: text, error: "" })}
        error={!!name.error}
        errorText={name.error}
      />

      <TextInput
        label="Email"
        returnKeyType="next"
        value={email.value}
        onChangeText={(text) => setEmail({ value: text, error: "" })}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      />

      <TextInput
        label="Mật khẩu"
        returnKeyType="done"
        value={password.value}
        onChangeText={(text) => setPassword({ value: text, error: "" })}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />

      <ButtonAction
        mode="contained"
        onPress={_onSignUpPressed}
        style={styles.button}
      >
        Sign Up
      </ButtonAction>

      <View style={styles.row}>
        <Text style={styles.label}>Bạn đã có tài khoản? </Text>
        <TouchableOpacity onPress={() => props.navigation.navigate("Login")}>
          <Text style={styles.link}>Đăng nhập</Text>
        </TouchableOpacity>
      </View>
    </Fragment>
  );
};
