import { DefaultTheme } from "react-native-paper";

export const colors = {
  ...DefaultTheme.colors,
  primary: "#600EE6",
  secondary: "#414757",
  error: "#f13a59",
  heavy: "#ff3535",
  little: "#13f27b",
  alway: "#f2d013",
};
