import { colors } from "./color";

export const fieldOption = {
  many: {
    vni: "Mưa nhiều",
    color: colors.heavy,
    level: 1,
  },
  little: {
    vni: "Mưa ít",
    color: colors.little,
    level: 0.033,
  },
  alway: {
    vni: "Có mưa",
    color: colors.alway,
    level: 0.08,
  },
};
