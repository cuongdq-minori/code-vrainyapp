import { StyleSheet } from "react-native";
import { colors } from "../../../constants/color";
export const styles = StyleSheet.create({
  container: {
    width: "100%",
    marginVertical: 12,
  },
  input: {
    backgroundColor: colors.surface,
  },
  error: {
    fontSize: 14,
    color: colors.error,
    paddingHorizontal: 4,
    paddingTop: 4,
  },
});
