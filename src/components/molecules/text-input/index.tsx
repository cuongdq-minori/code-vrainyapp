import React from "react";
import { Text, View } from "react-native";
import { BaseInput } from "../../atoms/input";
import { colors } from "../../constants/color";
import { styles } from "./style";

export const TextInput = ({ errorText, ...props }) => (
  <View style={styles.container}>
    <BaseInput
      style={styles.input}
      selectionColor={colors.primary}
      underlineColor="transparent"
      mode="outlined"
      {...props}
    />
    {errorText ? <Text style={styles.error}>{errorText}</Text> : null}
  </View>
);
