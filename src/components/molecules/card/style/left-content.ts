import { StyleSheet } from "react-native";
export const styles = StyleSheet.create({
  leftContent: { flexDirection: "column" },
  arroundRainy: { fontSize: 30, alignSelf: "center" },
  textUnit: { color: "black", fontSize: 15, alignSelf: "center" },
});
