import { StyleSheet } from "react-native";
export const styles = StyleSheet.create({
  cardBody: { justifyContent: "center" },
  titleCardBody: { fontSize: 16 },
});
