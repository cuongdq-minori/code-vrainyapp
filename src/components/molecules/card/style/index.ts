import { StyleSheet } from "react-native";
export const styles = StyleSheet.create({
  leftContentCard: { width: "15%", justifyContent: "center" },
  cardBody: { justifyContent: "center" },
});
