import React from "react";
import { StyleProp, ViewStyle } from "react-native";
import { Card } from "react-native-paper";
import { Provinces } from "../../../interfaces/interface";
import { BadgeStatus } from "../../atoms/badge/index";
import { fieldOption } from "../../constants/fieldOption";
import { CardBody } from "./components/BodyContent";
import { LeftContent } from "./components/LeftContent";
import { styles } from "./style";

interface CardDetailProps {
  detail: Provinces;
  style?: StyleProp<ViewStyle>;
}

export const CardDetail = ({ detail, style }: CardDetailProps) => (
  <Card style={style}>
    <Card.Title
      title={detail.name}
      subtitle={
        <BadgeStatus
          visible={true}
          size={25}
          statusRain={fieldOption[detail.status].vni}
          color={fieldOption[detail.status].color}
        />
      }
      left={() => (
        <LeftContent rainyAround={detail.rainyAround} status={detail.status} />
      )}
      leftStyle={styles.leftContentCard}
    />
    <Card.Content style={styles.cardBody}>
      <CardBody area={detail.area} villageName={detail.villageName} />
    </Card.Content>
  </Card>
);
