import React from "react";
import { Text, View } from "react-native";
import { fieldOption } from "../../../constants/fieldOption";
import { styles } from "../style/left-content";

interface LeftContentProps {
  status: string;
  rainyAround?: number;
}
export const LeftContent = (props: LeftContentProps): JSX.Element => {
  const { rainyAround, status } = props;
  return (
    <View style={styles.leftContent}>
      <Text style={[styles.arroundRainy, { color: fieldOption[status].color }]}>
        {rainyAround}
      </Text>
      <Text style={styles.textUnit}>(mm)</Text>
    </View>
  );
};
