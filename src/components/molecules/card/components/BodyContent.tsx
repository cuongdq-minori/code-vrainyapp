import React, { Fragment } from "react";
import { Paragraph, Title } from "react-native-paper";
import { styles } from "../style/body-content";

interface CardBodyProps {
  villageName: string;
  area?: string;
}

export const CardBody = ({ villageName, area }: CardBodyProps) => (
  <Fragment>
    <Title style={styles.titleCardBody}>Phường/Xã:{villageName}</Title>
    <Paragraph>Khu vực: {area}</Paragraph>
  </Fragment>
);
