import React from "react";
import { ImageBase } from "../../atoms/image";

export const Logo = () => (
  <ImageBase source={require("../../../assets/rainyAround.png")} />
);
