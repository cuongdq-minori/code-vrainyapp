import React from "react";
import { Text, View } from "react-native";
import { List } from "react-native-paper";
import { IconSource } from "react-native-paper/lib/typescript/src/components/Icon";
import { styles } from "../style/left-item";

interface LeftItemProps {
  rainyAround?: number;
  icon: IconSource;
  color?: string;
}
export const LeftItem = (props: LeftItemProps): JSX.Element => {
  const { icon, rainyAround, color } = props;
  return (
    <View style={styles.listLeftContent}>
      <List.Icon style={styles.listLeftIcon} icon={icon} color={color} />
      <Text style={styles.listLeftText}>{rainyAround}mm</Text>
    </View>
  );
};
