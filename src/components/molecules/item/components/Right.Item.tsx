import React from "react";
import { List } from "react-native-paper";
import { IconSource } from "react-native-paper/lib/typescript/src/components/Icon";
import { styles } from "../style/right-item";

interface RightItemProps {
  icon: IconSource;
  color: string;
}
export const RightItem = ({ icon, color }: RightItemProps): JSX.Element => {
  return <List.Icon style={styles.listRightIcon} icon={icon} color={color} />;
};
