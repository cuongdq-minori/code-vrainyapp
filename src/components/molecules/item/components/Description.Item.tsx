import React, { Fragment } from "react";
import { Text } from "react-native";
import { ProgressBar } from "react-native-paper";
import { fieldOption } from "../../../constants/fieldOption";
import { styles } from "../style/description-item";

type DescriptionItemProps = {
  status: string;
};
export const DescriptionItem = ({
  status,
}: DescriptionItemProps): JSX.Element => {
  return (
    <Fragment>
      <ProgressBar
        progress={fieldOption[status].level}
        color={fieldOption[status].color}
        style={styles.listProgressbar}
      />
      <Text style={styles.listText}>{fieldOption[status].vni}</Text>
    </Fragment>
  );
};
