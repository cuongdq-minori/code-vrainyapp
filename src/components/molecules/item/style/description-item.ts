import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  listProgressbar: { width: 30, marginTop: 10 },
  listText: { color: "white" },
});
