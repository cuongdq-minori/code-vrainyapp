import { StyleSheet } from "react-native";
import { colors } from "../../../constants/color";

export const styles = StyleSheet.create({
  label: {
    color: colors.secondary,
  },
  button: {
    marginTop: 24,
  },
  row: {
    flexDirection: "row",
    marginTop: 4,
  },
  link: {
    fontWeight: "bold",
    color: colors.primary,
  },
  //list item
  listItem: {
    margin: 5,
    backgroundColor: "#6d7893",
    borderRadius: 5,
  },
  listTitle: { color: "#ffffff", fontSize: 20 },
});
