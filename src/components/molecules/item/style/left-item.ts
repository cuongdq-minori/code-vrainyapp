import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  listLeftContent: {
    width: 80,
    marginLeft: 10,
    borderRightWidth: 1,
    borderColor: "#8991a3",
  },
  listLeftText: { marginTop: 1, color: "#ffffff" },
  listLeftIcon: {
    borderRightColor: "white",
    marginBottom: 0,
  },
});
