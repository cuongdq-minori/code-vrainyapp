import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  listRightIcon: { alignSelf: "center", marginRight: 0 },
});
