import React from "react";
import { List } from "react-native-paper";
import { Any } from "../../../interfaces/Unknown";
import { TextHeader } from "../../atoms/text";
import { fieldOption } from "../../constants/fieldOption";
import { DescriptionItem } from "./components/Description.Item";
import { LeftItem } from "./components/Left.Item";
import { RightItem } from "./components/Right.Item";
import { styles } from "./style";

interface ItemProps {
  data: Any;
  onPress: (value: Any) => void;
}
export const Item = ({ data, onPress }: ItemProps): JSX.Element => {
  return (
    <List.Item
      onPress={() => onPress(data)}
      style={styles.listItem}
      title={<TextHeader>{data?.name}</TextHeader>}
      titleStyle={styles.listTitle}
      left={() => (
        <LeftItem
          icon="cloud"
          color={fieldOption[data.status].color}
          rainyAround={data.rainyAround}
        />
      )}
      description={() => <DescriptionItem status={data.status} />}
      right={() => <RightItem icon="chevron-right" color="white" />}
    />
  );
};
