import { Fragment } from "react";
import React from "react-native";
import Tag from "../../atoms/tag";
type TagSelectProps = {
  data: string[];
};
export default function TagSelect({ data }: TagSelectProps): JSX.Element {
  return (
    <Fragment>
      {data.map((tag, index) => {
        <Tag key={index}>{tag}</Tag>;
      })}
    </Fragment>
  );
}
