import React from "react";
import { Badge } from "react-native-paper";

interface BadgeStatusProps {
  statusRain: string | number;
  color?: string;
  visible: boolean;
  size?: number;
}
export const BadgeStatus = (props: BadgeStatusProps): JSX.Element => {
  const { visible, statusRain, color, size } = props;
  return (
    <Badge
      visible={visible}
      size={size}
      style={{ backgroundColor: color }}
      children={statusRain}
    />
  );
};
