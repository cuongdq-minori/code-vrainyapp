import React, { ReactNode } from "react";
import {
  ImageBackground,
  ImageResizeMode,
  ImageSourcePropType,
  KeyboardAvoidingView,
} from "react-native";
import { styles } from "./style";

type Base = {
  children?: ReactNode;
  source: ImageSourcePropType;
  resizeMode?: ImageResizeMode;
};
export const BackgroundBase = ({ resizeMode, source, children }: Base) => (
  <ImageBackground
    source={source}
    resizeMode={resizeMode}
    style={styles.background}
  >
    <KeyboardAvoidingView style={styles.container} behavior="padding">
      {children}
    </KeyboardAvoidingView>
  </ImageBackground>
);
