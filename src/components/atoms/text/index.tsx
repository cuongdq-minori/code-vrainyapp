import React from "react";
import { Text } from "react-native";
import { styles } from "./style";

type TextBase = {
  children?: string;
  StyleSheet?: object;
};
export const TextHeader = ({ children, StyleSheet }: TextBase) => (
  <Text style={StyleSheet}>{children}</Text>
);
export const TextParagraph = ({ children }) => (
  <Text style={styles.textParaph}>{children}</Text>
);
