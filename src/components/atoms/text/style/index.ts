import { StyleSheet } from "react-native";
import { colors } from "../../../constants/color";
export const styles = StyleSheet.create({
  textParaph: {
    fontSize: 16,
    lineHeight: 26,
    color: colors.secondary,
    textAlign: "center",
    marginBottom: 14,
  },
});
