import React from "react";
import { Image, ImageSourcePropType } from "react-native";
import { styles } from "./style";

type ImageBase = {
  source: ImageSourcePropType;
};
export const ImageBase = ({ source }: ImageBase) => (
  <Image source={source} style={styles.image} />
);
