import React from "react";
import { StyleProp, TextStyle } from "react-native";
import { Button as IconBtn, Button as PaperButton } from "react-native-paper";
import { IconSource } from "react-native-paper/lib/typescript/src/components/Icon";
import { colors } from "../../constants/color";
import styles from "../button/style/index";

type ButtonBaseProps = {
  icon?: IconSource;
  children?: React.ReactNode;
  mode?: "text" | "outlined" | "contained";
  color?: string;
  loading?: boolean;
  labelStyle?: StyleProp<TextStyle>;
  onPress?: () => void;
  style?: StyleProp<TextStyle>;
};

export const ButtonAction = ({ mode, children, ...props }) => (
  <PaperButton
    loading={props.loading}
    style={[
      styles.button,
      mode === "outlined" && { backgroundColor: colors.surface },
      props.style,
    ]}
    disabled={props.loading}
    labelStyle={styles.text}
    mode={mode}
    {...props}
  >
    {children}
  </PaperButton>
);

export const IconButton = (props: ButtonBaseProps) => {
  const {
    icon,
    loading,
    style,
    children,
    mode,
    color,
    labelStyle,
    onPress,
  } = props;
  return (
    <IconBtn
      {...props}
      style={style}
      loading={loading}
      icon={icon}
      mode={mode}
      children={children}
      color={color}
      labelStyle={labelStyle}
      onPress={onPress}
      compact={true}
    />
  );
};
