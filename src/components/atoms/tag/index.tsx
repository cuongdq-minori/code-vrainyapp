import React, { Children } from "react";
import { Chip } from "react-native-paper";
import { styles } from "./style";

type TagProps = {};
export default function Tag({}: TagProps): JSX.Element {
  const [selected, setSelected] = React.useState(false);

  return (
    <Chip
      selected={selected}
      selectedColor="green"
      style={styles.marginTag}
      onPress={() => setSelected(!selected)}
    >
      {Children}
    </Chip>
  );
}
