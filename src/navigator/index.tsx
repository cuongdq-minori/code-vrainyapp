import { NavigationContainer, RouteProp } from "@react-navigation/native";
import {
  createStackNavigator,
  StackNavigationProp,
} from "@react-navigation/stack";
import React from "react";
import { Detail } from "../components/pages/detail-district/DetailDistrict";
import { ForgotPassword } from "../components/pages/forgot-password/ForgotPassWord";
import { Home } from "../components/pages/home/index";
import { DistrictList } from "../components/pages/list-district/DistrictList";
import { LoginAuthor } from "../components/pages/login/index";
import Register from "../components/pages/register/Register";
import Search from "../components/pages/search";
import { Provinces } from "../interfaces/interface";

export type RootStackParamList = {
  Login: undefined;
  ForgotPass: undefined;
  Register: undefined;
  Home: undefined;
  DistrictList: { headerName: string; item?: Provinces[] };
  Detail: { headerName: string; detail: Provinces };
  Search: undefined;
};

export type LoginProps = {
  route: RouteProp<RootStackParamList, "Login">;
  navigation: StackNavigationProp<RootStackParamList, "Login">;
};

export type ForgotPassProps = {
  route: RouteProp<RootStackParamList, "ForgotPass">;
  navigation: StackNavigationProp<RootStackParamList, "ForgotPass">;
};

export type RegisterProps = {
  route: RouteProp<RootStackParamList, "Register">;
  navigation: StackNavigationProp<RootStackParamList, "Register">;
};

export type HomeProps = {
  route: RouteProp<RootStackParamList, "Home">;
  navigation: StackNavigationProp<RootStackParamList, "Home">;
};

export type SearchProps = {
  route: RouteProp<RootStackParamList, "Search">;
  navigation: StackNavigationProp<RootStackParamList, "Search">;
};

export type DistrictListProps = {
  route: RouteProp<RootStackParamList, "DistrictList">;
  navigation: StackNavigationProp<RootStackParamList, "DistrictList">;
};

export type DetailProps = {
  route: RouteProp<RootStackParamList, "Detail">;
  navigation: StackNavigationProp<RootStackParamList, "Detail">;
};

const Stack = createStackNavigator();

export function AppContainer(): JSX.Element {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={"Home"}
        screenOptions={
          {
            // gestureEnabled: true,
          }
        }
      >
        <Stack.Screen
          name="Login"
          component={LoginAuthor}
          options={{
            title: "VRainy",
          }}
        />

        <Stack.Screen
          name="Home"
          component={Home}
          options={{
            title: "Trang chủ",
            headerTintColor: "white",
            headerStyle: { backgroundColor: "#2f4777" },
          }}
        />
        <Stack.Screen
          name="DistrictList"
          component={DistrictList}
          options={{
            headerTintColor: "white",
            headerStyle: { backgroundColor: "#2f4777" },
          }}
        />
        <Stack.Screen
          name="Detail"
          component={Detail}
          options={{
            headerTintColor: "white",
            headerStyle: { backgroundColor: "#2f4777" },
          }}
        />
        <Stack.Screen
          name="ForgotPass"
          component={ForgotPassword}
          options={{
            title: "Quên mật khẩu",
          }}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{
            title: "Đăng kí ",
          }}
        />

        <Stack.Screen
          name="Search"
          component={Search}
          options={{
            title: "Search",
            headerTintColor: "white",
            headerStyle: { backgroundColor: "#2f4777" },
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
